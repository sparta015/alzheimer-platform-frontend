import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from './login.service';
import { User } from '../user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  goodUserPasswd : boolean = true;

  notFieldsEmpty : boolean = true;

  userNotInSistemOrWrongPasswd : boolean = true;

  submitted : boolean = false;

  user : any = {};

  userId: number;

  fieldValidation(user: User) : boolean {
    if (user.email === "undefined"
      || user.password === "undefined")
      return false;
    return true;
  }

  hasCorrectPasswordLenght(password : string) : boolean {
    if (password.length >= 8) {
      return true;
    }
    return false;
  }

  hasNumberInPassword(password : string) : boolean {
    var hasNumber = /\d/;
    return hasNumber.test(password);
  }

  hasCorrectFormatInPassword(password : string) : boolean {
    if (this.hasCorrectPasswordLenght(password) && 
        this.hasNumberInPassword(password)) {
      return true;
    }
    return false;
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private loginService : LoginService
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.user);
  }

  login() {
    this.notFieldsEmpty = this.fieldValidation(this.user);
    if (this.notFieldsEmpty) {
      this.goodUserPasswd = this.hasCorrectFormatInPassword(this.user.password);
      if (this.goodUserPasswd) {
        //This operation is done after server respone
        //Send data to the server
        this.loginService.loginUser(this.user);
        //Validation
        this.userNotInSistemOrWrongPasswd = false;
        localStorage.setItem("user", "patient");
        console.log(localStorage.getItem("user"))
        this.router.navigate(['menu']);
      }
    }
  }
}
