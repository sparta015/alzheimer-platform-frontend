import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { User } from '../user';

@Injectable()
export class LoginService {

  userId: number;

  urlLogin : string = 'localhost:4200/menu'

  httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
  };

  loginUser(user: User): void {
      //return this.http.post<User>(this.urlLogin, user, this.httpOptions);
  }

  constructor(private http: HttpClient) { }

}
