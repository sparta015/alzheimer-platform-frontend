import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Developer } from '../developer';

@Injectable()
export class RegistrationDevService {

  developer : Developer;

  urlRegistrationDev : string = 'http://localhost:9000/register/developer';

  httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
  };

  sendDeveloper(developer: any): Observable<Developer> {
    console.log(this.urlRegistrationDev);
    return this.httpClient.post<Developer>(this.urlRegistrationDev, developer, this.httpOptions);
  }

  constructor(private httpClient: HttpClient) { }

}
