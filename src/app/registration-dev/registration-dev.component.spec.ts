import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationDevComponent } from './registration-dev.component';

describe('RegistrationDevComponent', () => {
  let component: RegistrationDevComponent;
  let fixture: ComponentFixture<RegistrationDevComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrationDevComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationDevComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
