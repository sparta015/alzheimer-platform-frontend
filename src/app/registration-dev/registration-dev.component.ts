import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RegistrationDevService } from './registration-dev.service'
import { Developer } from '../developer';

@Component({
  selector: 'app-registration-dev',
  templateUrl: './registration-dev.component.html',
  styleUrls: ['./registration-dev.component.css']
})
export class RegistrationDevComponent implements OnInit {

  submitted = false;

  newDeveloper : any = {};

  developer : any = {};

  serverDev : any = {};

  passEquals : boolean = true;

  goodPasswd : boolean = true;

  goodRepPasswd : boolean = true;

  fieldRequiredValidation : boolean = true;

  notNew: boolean = true;

  sendNewDeveloper(newDeveloper : any) : any {
    //make the request
    console.log("sending")
    console.log(this.newDeveloper.passwd);
    //this.registerDevService.sendDeveloper(newDeveloper).subscribe((developer : Developer) => this.serverDev = developer);
    this.registerDevService.sendDeveloper(newDeveloper).subscribe(developer => this.serverDev = developer);
  }

  hasCorrectPasswordLenght(password : string) : boolean {
    if (password.length >= 8) {
      return true;
    }
    return false;
  }

  hasNumberInPassword(password : string) : boolean {
    var hasNumber = /\d/;
    return hasNumber.test(password);
  }

  hasCorrectFormatInPassword(password : string) : boolean {
    if (this.hasCorrectPasswordLenght(password) && 
        this.hasNumberInPassword(password)) {
      return true;
    }
    return false;
  }

  areEqualsPassword(password : string, confirmPassword : string) : boolean {
    if (password === confirmPassword) {
      return true;
    }
    return false;
  }

  fieldValidation(developer: Developer) : boolean {
    if (developer.passwd === "undefined"
      || developer.confirmPassword === "undefined"
      || developer.email === "undefined")
      return false;
    return true;
  }

  buildDeveloperToSend(developer: Developer) : void {
    console.log(this.developer.email);
    this.newDeveloper.email = this.developer.email;
    this.newDeveloper.passwd = this.developer.passwd;
  }
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private registerDevService: RegistrationDevService
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.newDeveloper);
  }

  clicked() {
    console.log(this.developer);
    this.fieldRequiredValidation = this.fieldValidation(this.developer);
    console.log(this.fieldRequiredValidation);
    if (this.fieldRequiredValidation) {
      this.goodPasswd = this.hasCorrectFormatInPassword(this.developer.passwd);
      this.goodRepPasswd = this.hasCorrectFormatInPassword(this.developer.passwordRep);
      if (!this.areEqualsPassword(this.developer.passwd, this.developer.passwordRep)) {
        console.log("Las passwd son distintas")
        this.passEquals = false;
      } else {
        console.log("Las passwd son iguales")
        this.passEquals = true;
      }
      console.log(this.goodPasswd, this.goodRepPasswd, this.passEquals);
      //Send data to server for registration
      console.log(this.developer);
      console.log("send data to server")
      this.buildDeveloperToSend(this.developer);
      this.sendNewDeveloper(this.newDeveloper);
      localStorage.setItem("user", "dev");
      localStorage.setItem("email", this.newDeveloper.email)
      this.router.navigate(['registration/dev/success']);
    }
  }

}
