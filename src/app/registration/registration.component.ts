import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Developer } from '../developer';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  message = "Selecciona una opción:";

  registerUser() : void {
    //TODO reditect to user form
  };

  registerDev() : void {
    //TODO redirect to developer form
  };
  
  constructor() { }

  ngOnInit() {
  }

}
