import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Developer } from '../developer';
import { Router, ActivatedRoute } from '@angular/router';
import { RegistrationDevSuccessService } from './registration-dev-success.service';

@Component({
  selector: 'app-registration-dev-success',
  templateUrl: './registration-dev-success.component.html',
  styleUrls: ['./registration-dev-success.component.css']
})

export class RegistrationDevSuccessComponent implements OnInit {

  newDeveloper : Developer;

  getNewDeveloper(): void {
    this.registrationDevSuccessService.getDeveloper()
        .subscribe(newDeveloper => this.newDeveloper = newDeveloper);
    
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private registrationDevSuccessService : RegistrationDevSuccessService) { }

  ngOnInit() {
    this.getNewDeveloper();
  }

  clicked() {
    this.router.navigate(['menu']);
  }

}
