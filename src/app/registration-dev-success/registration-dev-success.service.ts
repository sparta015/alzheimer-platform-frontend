import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Developer } from '../developer';
import { DEVELOPERFAKE } from '../developer-fake';


@Injectable()
export class RegistrationDevSuccessService {

  getDeveloper(): Observable<Developer> {
    return of(DEVELOPERFAKE);
  }

  constructor() { }

}
