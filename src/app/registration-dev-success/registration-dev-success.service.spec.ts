import { TestBed, inject } from '@angular/core/testing';

import { RegistrationDevSuccessService } from './registration-dev-success.service';

describe('RegistrationDevService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RegistrationDevSuccessService]
    });
  });

  it('should be created', inject([RegistrationDevSuccessService], (service: RegistrationDevSuccessService) => {
    expect(service).toBeTruthy();
  }));
});
