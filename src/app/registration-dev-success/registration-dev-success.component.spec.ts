import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationDevSuccessComponent } from './registration-dev-success.component';

describe('RegistrationDevSuccessComponent', () => {
  let component: RegistrationDevSuccessComponent;
  let fixture: ComponentFixture<RegistrationDevSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrationDevSuccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationDevSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
