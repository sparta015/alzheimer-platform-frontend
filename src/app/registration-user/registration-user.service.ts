import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Patient } from '../patient';

@Injectable()
export class RegistrationUserService {

  patient : Patient;

  urlRegistrationPatient : string = 'http://localhost:9000/register/patient'

  httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json'
      })
  };

  sendDeveloper(patient: Patient): Observable<Patient> {
      return this.http.post<Patient>(this.urlRegistrationPatient, patient, this.httpOptions);
  }

  constructor(private http: HttpClient) { }

}
