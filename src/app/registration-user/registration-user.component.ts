import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RegistrationUserService } from './registration-user.service'
import { Patient } from '../patient';

@Component({
  selector: 'app-registration-user',
  templateUrl: './registration-user.component.html',
  styleUrls: ['./registration-user.component.css']
})
export class RegistrationUserComponent implements OnInit {

  newPatient: any = {};

  patient: any = {};

  fieldRequiredValidation: boolean = true;

  goodPasswd: boolean = true;

  goodRepPasswd: boolean = true;

  passEquals: boolean = true;

  submitted = false;

  notUserInSystem : boolean = true;

  sendNewPatient(patient : Patient) : void {
    this.registerPatientService.sendDeveloper(patient).subscribe(patient => console.log(patient));
  }

  fieldValidation(patient: Patient) : boolean {
    if (patient.email === "undefined"
      || patient.password === "undefined"
      || patient.repPassword === "undefined"
      || patient.country === "undefined"
      || patient.region === "undefined")
      return false;
    return true;
  }

  buildPatientToSend(patient: Patient) : void {
    this.newPatient.email = this.patient.email;
    this.newPatient.passwd = this.patient.password;
    this.newPatient.country = this.patient.country;
    this.newPatient.region = this.patient.region;
    this.newPatient.age = this.patient.age;
  }

  hasCorrectPasswordLenght(password : string) : boolean {
    if (password.length >= 8) {
      return true;
    }
    return false;
  }

  hasNumberInPassword(password : string) : boolean {
    var hasNumber = /\d/;
    return hasNumber.test(password);
  }

  hasCorrectFormatInPassword(password : string) : boolean {
    if (this.hasCorrectPasswordLenght(password) && 
        this.hasNumberInPassword(password)) {
      return true;
    }
    return false;
  }

  areEqualsPassword(password : string, confirmPassword : string) : boolean {
    if (password === confirmPassword) {
      return true;
    }
    return false;
  }

  registerPatient(newPatient: Patient) {
    //TODO make the request
    this.patient = newPatient;
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private registerPatientService: RegistrationUserService
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.newPatient);
  }

  clicked() {
    console.log(this.patient);
    this.fieldRequiredValidation = this.fieldValidation(this.patient);
    console.log(this.fieldRequiredValidation);
    if (this.fieldRequiredValidation) {
      this.goodPasswd = this.hasCorrectFormatInPassword(this.patient.password);
      this.goodRepPasswd = this.hasCorrectFormatInPassword(this.patient.passwordRep);
      if (!this.areEqualsPassword(this.patient.password, this.patient.passwordRep)) {
        console.log("Las passwd son distintas")
        this.passEquals = false;
      } else {
        console.log("Las passwd son iguales")
        this.passEquals = true;
      }
      console.log(this.goodPasswd, this.goodRepPasswd, this.passEquals);
      //Send data to server for registration
      this.buildPatientToSend(this.patient);
      console.log(this.newPatient)
      this.sendNewPatient(this.newPatient);
      localStorage.setItem("user", "patient");
      this.router.navigate(['menu']);
    }
  }

}
