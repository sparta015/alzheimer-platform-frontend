import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Game } from '../game';

@Injectable()
export class PublishService {

  urlRegistrationGame : string = 'http://localhost:9000/games'

  httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
  };

  sendGame(game: any): Observable<Game> {
    console.log("envio para el server")
    console.log(this.httpOptions);
    console.log(game);
    console.log(this.urlRegistrationGame)
      return this.http.post<Game>(this.urlRegistrationGame, game, this.httpOptions);
  }

  constructor(private http: HttpClient) { }

}
