import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PublishService } from './publish.service'
import { Game } from '../game';

@Component({
  selector: 'publish',
  templateUrl: './publish.component.html',
  styleUrls: ['./publish.component.css']
})
export class PublishComponent implements OnInit {

  submitted = false;

  newGame : any;

  game : any = {};

  fieldRequiredValidation: boolean = true;

  sendNewGame(game : any) : any {
    //make the request
    console.log(game);
    this.publishService.sendGame(game).subscribe(game => console.log(game.name));
  }

  fieldValidation(game: Game) : boolean {
    if (game.name === "undefined"
      || game.link === "undefined"
      || game.description === "undefined")
      return false;
    return true;
  }
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private publishService: PublishService
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.newGame);
  }

  clicked() {
    console.log(this.game);
    this.fieldRequiredValidation = this.fieldValidation(this.game);
    console.log(this.fieldRequiredValidation);
    if (this.fieldRequiredValidation) {
      //Send data to server for registration
      console.log(localStorage.getItem("email"));
      this.game.email = localStorage.getItem("email");
      this.sendNewGame(this.game);
      localStorage.setItem("user", "dev");
      this.router.navigate(['menu']);
    }
  }

}
