export class Game {
  name: string;
  link: string;
  description: string;
  email: string;
}