import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { Game } from '../game';
import { GAME_LIST} from '../game-list';
import { GAME_LIST_DEV} from '../game-list2';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class MenuService {

  gameSearch : Game[];

  getExercisesUrl: string = 'http://localhost:9000/games';

  httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

  getGameList(userId: number): Observable<Game[]> {
    if (userId % 2 === 0) {
      return of(GAME_LIST_DEV);
    } else {
      return of(GAME_LIST);
    }
    //return this.http.get(this.getExercisesUrl + id).map((res: Response) => res.json);
  }

  getDevGameList(email: string): Observable<Game[]> {
    return this.http.get<Game[]>(this.getExercisesUrl.concat("/") + btoa(email), this.httpOptions);
  }
  getGameListByName(name: string): Observable<Game[]> {
    return of(GAME_LIST.filter(game => game.name === name));
  }

  getAllGameList(): Observable<Game[]> {
    return this.http.get<Game[]>(this.getExercisesUrl, this.httpOptions);
  }

  constructor(private http: HttpClient) { }

}
