import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Game } from '../game';
import { GAME_LIST } from '../game-list';
import { MenuService } from './menu.service';
import { User } from '../user';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})

export class MenuComponent implements OnInit {

  gameList : Game[];

  pagination : number;

  isDev : boolean = false;

  type: string;

  emailDev: string;

  //Move to another element
  showBotton(user: string): boolean {
    if ( user === localStorage.getItem("user")) return true;
    return false;
  }

  searchGame(name: string): void {
    this.menuService.getGameListByName(name).subscribe(gamesSearch => this.gameList = gamesSearch);
  }

  getGameList(): void {
    this.type = localStorage.getItem("user");
    if (this.type === "dev") {
      this.emailDev = localStorage.getItem("email");
      this.menuService.getDevGameList(this.emailDev).subscribe(gameList => this.gameList = gameList);
    } else {
      this.menuService.getAllGameList().subscribe(allGameList => this.gameList = allGameList);
    }
  }

  constructor(private menuService: MenuService) { }

  ngOnInit() {
    this.getGameList();
    this.isDev = this.showBotton("dev");
    console.log(this.isDev);
  }

}
