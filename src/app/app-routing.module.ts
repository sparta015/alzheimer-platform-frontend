import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrationComponent} from './registration/registration.component';
import { RegistrationDevComponent } from './registration-dev/registration-dev.component';
import { RegistrationUserComponent } from './registration-user/registration-user.component';
import { LoginComponent } from './login/login.component';
import { RegistrationDevSuccessComponent } from './registration-dev-success/registration-dev-success.component';
import { MenuComponent } from './menu/menu.component';
import {PublishComponent} from './publish/publish.component';

const routes: Routes = [
  { path: 'registration', component: RegistrationComponent },
  { path: 'registration/dev', component: RegistrationDevComponent },
  { path: 'registration/user', component: RegistrationUserComponent },
  { path: 'registration/dev/success', component: RegistrationDevSuccessComponent },
  { path: 'login', component: LoginComponent },
  { path: 'menu', component: MenuComponent},
  { path: 'publish', component: PublishComponent},
  { path: '', redirectTo: '/login', pathMatch: 'full'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {
  
 }
