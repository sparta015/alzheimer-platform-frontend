export class Patient {
  email: string;
  password: string;
  repPassword: string;
  age: number;
  country: string;
  region: string;
}
