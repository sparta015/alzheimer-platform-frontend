import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './/app-routing.module';
import { RegistrationComponent } from './registration/registration.component';
import { RegistrationDevComponent } from './registration-dev/registration-dev.component';
import { RegistrationUserComponent } from './registration-user/registration-user.component';
import { RegistrationDevSuccessComponent } from './registration-dev-success/registration-dev-success.component';
import { PublishComponent } from './publish/publish.component'

import { RegistrationDevSuccessService } from './registration-dev-success/registration-dev-success.service';
import { MenuService } from './menu/menu.service';
import { LoginService } from './login/login.service';
import { RegistrationUserService} from './registration-user/registration-user.service';
import { RegistrationDevService } from './registration-dev/registration-dev.service';
import { RecoverPasswdComponent } from './recover-passwd/recover-passwd.component';
import { PublishService } from './publish/publish.service';


@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    MenuComponent,
    LoginComponent,
    RegistrationDevComponent,
    RegistrationUserComponent,
    RegistrationDevSuccessComponent,
    RecoverPasswdComponent,
    PublishComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    RegistrationDevSuccessService,
    MenuService,
    LoginService,
    RegistrationUserService,
    RegistrationDevService,
    PublishService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
