import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecoverPasswdComponent } from './recover-passwd.component';

describe('RecoverPasswdComponent', () => {
  let component: RecoverPasswdComponent;
  let fixture: ComponentFixture<RecoverPasswdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecoverPasswdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoverPasswdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
